﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostCamera : MonoBehaviour
{

    public float cameraSensitivity = 90;
    public float climbSpeed = 4; 
    public float normalMoveSpeed = 10;
    public float fastMoveFactor = 3;

    public float rotationX = 0.0f;
    public float rotationY = 0.0f;

    void Start()
    {
        //Cache le cursor et le centre au milieu de l'écran
        Screen.lockCursor = true;
    }

    void Update()
    {
        //Quand on bouge la souris la caméra suit le curseur
        rotationX += Input.GetAxis("Mouse X") * cameraSensitivity * Time.deltaTime;
        rotationY += Input.GetAxis("Mouse Y") * cameraSensitivity * Time.deltaTime;
        rotationY = Mathf.Clamp(rotationY, -90, 90);

        transform.localRotation = Quaternion.AngleAxis(rotationX, Vector3.up);
        transform.localRotation *= Quaternion.AngleAxis(rotationY, Vector3.left);

        //Si on appuie sur shift et Z on avance plus vite 
        if (Input.GetKey(KeyCode.LeftShift))
        {
            transform.position += transform.forward * (normalMoveSpeed * fastMoveFactor) * Input.GetAxis("Vertical") * Time.deltaTime;
            transform.position += transform.right * (normalMoveSpeed * fastMoveFactor) * Input.GetAxis("Horizontal") * Time.deltaTime;
        }

        //Sinon, la caméra bouge à vitesse normal
        else
        {
            transform.position += transform.forward * normalMoveSpeed * Input.GetAxis("Vertical") * Time.deltaTime;
            transform.position += transform.right * normalMoveSpeed * Input.GetAxis("Horizontal") * Time.deltaTime;
        }

        //Si on appuie sur espace, on monte
        if (Input.GetKey(KeyCode.Space))
        {
            transform.position += transform.up * climbSpeed * Time.deltaTime;
        }

        //Si on appuie sur control, on descend
        if (Input.GetKey(KeyCode.LeftControl))
        {
            transform.position -= transform.up * climbSpeed * Time.deltaTime;
        }

    }
}
