﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchCamera : MonoBehaviour
{
    public GameObject ghostCamera;
    public GameObject fpsCamera;

    public GameObject player; 
    void Start()
    {
        //Initialise la position de la caméra
        cameraPositionChange(PlayerPrefs.GetInt("CameraPosition"));

    }

    void Update()
    {
        switchCamera();
    }

    //Quand on appuie sur A, change de caméra
    void switchCamera()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            cameraChangeCounter();  
        }
    }

    //Compteur de la caméra
    void cameraChangeCounter()
    {
        int cameraPositionCounter = PlayerPrefs.GetInt("CameraPosition");
        cameraPositionCounter++;
        cameraPositionChange(cameraPositionCounter);
    }

    //Change la position de la caméra
    void cameraPositionChange(int camPosition)
    {
        if (camPosition > 1)
        {
            camPosition = 0;
        }

        //Enregistre la position de la caméra
        PlayerPrefs.SetInt("CameraPosition", camPosition);

        //Caméra à la position 1
        if (camPosition == 0)
        {
            player.SetActive(false);

            ghostCamera.SetActive(true);

            fpsCamera.SetActive(false);
        }

        //Caméra à la position 2
        if (camPosition == 1)
        {
            player.SetActive(true);

            fpsCamera.SetActive(true);

            ghostCamera.SetActive(false);
        }

    }
}
