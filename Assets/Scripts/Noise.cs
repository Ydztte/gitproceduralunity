﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Noise : MonoBehaviour
{
    //creation des noise map
    #region


    public int mapWidth;
    public int mapHeight;

    public float noiseScale;

    public Vector2 mapOrigin;
    public float OffsetX = 0;
    public float OffsetY = 0;

    public float[,] noiseMap = new float[0, 0];
    public float[,] noiseMap2 = new float[0, 0];

    public Color colorMin;
    public Color colorMax;

    public Color colorMin2;
    public Color colorMax2;

    private Color[] colorMap;


    private Color[] colorMap2;

    public Renderer textureRender;
    #endregion


    //contrast amplication et smooth
    #region
    [Range(0, 1)]
    public float contrastMin;
    [Range(0, 1)]
    public float contrastMax;


    [Range(0, 1)]
    public float contrastMin2;
    [Range(0, 1)]
    public float contrastMax2;

    [Range(0, 1)]
    public float amplification;
    public float amplificationFactor;

    [Range(0, 1)]
    public float smooth;
    public float smoothFactor;

    [Range(0, 1)]
    public float amplification2;
    public float amplificationFactor2;

    [Range(0, 1)]
    public float smooth2;

    public float smoothFactor2;
    #endregion
    

    //game object
    #region

    public GameObject grassCube;
    public GameObject dirtCube;
    public GameObject stoneCube;
    public GameObject waterCube;
    public GameObject tree;
    public GameObject herbe;
    public GameObject algues;
    public Material[] Mat;

    [HideInInspector]
    public List<GameObject> blocs;

    #endregion


    //parametrage des couches 
    #region

    [Range(0, 10)]
    public int densiteArbre;
    private bool biomeArbre = true;

    public AnimationCurve curve;


    public int dirtStrate;
    public int stoneStrate;
    public float niveaueau;

    //Génération caverne
    public float noiseFactorStone = 0.2f;

    #endregion

    //variable player
    #region

    public GameObject player;
    public float OffSpeed = 0.01f;
    #endregion



    public void Generate()
    {
        GenerateNoiseMap();
    }

    public void Start()
    {
        Generate();
        GenerateFloor();
    }
    private void Update()
    {
        Generate();

        if (Input.GetKeyDown("e"))
        {
            GenerateFloor();

            //changer bool arbre et densité arbre
            var nbrandomm = Random.Range(0, 1);
            if (nbrandomm == 1 )
            {
                biomeArbre = false;
            }
            else if (nbrandomm == 0)
            {
                biomeArbre = true;
            }

            densiteArbre = Random.Range(0, 10);
        }
    }


    public void GenerateNoiseMap()
    {
        noiseMap = new float[mapWidth, mapHeight];
        colorMap = new Color[mapWidth * mapHeight];

        for (int x = 0; x < mapWidth; x++)
        {
            for (int y = 0; y < mapHeight; y++)
            {
                float xCoord = mapOrigin.x + x / (mapWidth * noiseScale);
                float yCoord = mapOrigin.y + y / (mapHeight * noiseScale);
                float sample = Mathf.PerlinNoise(xCoord, yCoord);
                //float sampleAmplification = Mathf.PerlinNoise(xCoord, yCoord) * amplification;

                //sample = sample * contrastMin * amplification;
                sample = Mathf.Clamp(sample, contrastMin, contrastMax);
                sample = Mathf.InverseLerp(Mathf.Lerp(contrastMin, contrastMin / smoothFactor, smooth), Mathf.Lerp(contrastMax, contrastMax * amplificationFactor, amplification), sample);

                noiseMap[x, y] = sample;

                colorMap[y * mapWidth + x] = Color.Lerp(colorMin, colorMax, sample);

                


            }
        }


    }


    public void DrawNoiseMap()
    {
        //Création d'une nouvelle texture avec une ligne de nuances de gris
        Texture2D texture = new Texture2D(mapWidth, mapHeight);

        texture.SetPixels(colorMap);
        texture.Apply();

        //Affichage de la texture sur le material de l'objet
        textureRender.sharedMaterial.mainTexture = texture;
        textureRender.transform.localScale = new Vector3(mapWidth, 1, mapHeight);
    }

    //Création des blocs
    public void GenerateFloor()
    {
        blocs.Clear();

        for (int x = 0; x < mapWidth; x++)
        {



            //Génération des couches suppérieur eeau  + salbe 
            for (int yGrass = 0; yGrass < mapHeight; yGrass++)
            {

                //génération des arbres
                if (Mathf.Floor(noiseMap[x, yGrass] * 15) > 5 && Random.Range(0, 20) <= densiteArbre && biomeArbre)
                {
                    var newTree = Instantiate(tree);
                    newTree.transform.position = new Vector3(x+OffsetX,
                                                             (Mathf.Floor(noiseMap[x, yGrass] * 15) - 0.5f),
                                                             yGrass);
                    var nbRandom = Random.Range(0.4f, 3f); 
                    newTree.transform.localScale = new Vector3(nbRandom, nbRandom, nbRandom);
                }

                //intantiate des hebrs
                if (Mathf.Floor(noiseMap[x, yGrass] * 15) > 2 && Random.Range(0, 20) <= densiteArbre && biomeArbre)
                {
                    var newHerbe = Instantiate(herbe);
                    newHerbe.transform.position = new Vector3(x + OffsetX,
                                                             (Mathf.Floor(noiseMap[x, yGrass] * 15) - 0.5f),
                                                             yGrass);
                    var nbRandom = Random.Range(1,1);
                    newHerbe.transform.localScale = new Vector3(nbRandom, nbRandom, nbRandom);
                    newHerbe.transform.rotation = Quaternion.Euler(0f, Random.Range(0, 180), 0f);
                }

                //génération du gazon
                var grassCubeObject = Instantiate(grassCube);
                blocs.Add(grassCubeObject);
                grassCubeObject.transform.position = new Vector3(x + OffsetX, Mathf.Round(curve.Evaluate(noiseMap[x, yGrass]) * 5), yGrass);


                //génération de l'eau
                if (Mathf.Floor(noiseMap[x, yGrass] * 12) < niveaueau-1)
                {
                    for (float k = Mathf.Floor(noiseMap[x, yGrass] * 6) + 1; k < niveaueau; k++)
                    {
                        //Generate water
                        var neweau = Instantiate(waterCube);
                        neweau.transform.position = new Vector3(x + OffsetX, k, yGrass);
                    }


                    // algues intantiate
                    if (Mathf.Floor(noiseMap[x, yGrass] * 12) < niveaueau - 2 && Random.Range(1,10) <= 5)
                    {
                        var newAlgue = Instantiate(algues);
                        newAlgue.transform.position = new Vector3(x + OffsetX,
                                                                 (Mathf.Floor(noiseMap[x, yGrass] * 15) + 0.5f),
                                                                 yGrass);
                        var nbRandom = Random.Range(1, 1);
                        //ajout rotation
                        newAlgue.transform.localScale = new Vector3(nbRandom, nbRandom, nbRandom);
                    }
                }


                //modification des materiaux pour le sable
                if (Mathf.Floor(noiseMap[x, yGrass] * 8) > 1)
                {
                    //grassCubeObject.GetComponent<Renderer>().material = Mat[0]; // Génération de l'herbe au dessus de la couche 4
                }
                else
                {
                    grassCubeObject.GetComponent<Renderer>().material = Mat[1]; // Génération du sable inférieur ou égale à la couche 4
                }
            }

            //Génération du terre
            for (int yDirt = 0; yDirt < mapHeight; yDirt++)
            {
                for (int i = 0; i < dirtStrate; i++)
                {
                    var dirtCubeObject = Instantiate(dirtCube);
                    blocs.Add(dirtCubeObject);
                    dirtCubeObject.transform.position = new Vector3(x + OffsetX, Mathf.Round(curve.Evaluate(noiseMap[x, yDirt]) * 5) - i - 1, yDirt);
                }
            }

            //Génération de la roche
            for (int yStone = 0; yStone < mapHeight; yStone++)
            {
                for (int i = 0; i < stoneStrate; i++)
                {
                    if (Perlin3DCavern(x * noiseFactorStone, yStone * noiseFactorStone, i * noiseFactorStone) >= 0.45f)
                    {
                        var stoneCubeObject = Instantiate(stoneCube);
                        blocs.Add(stoneCubeObject);
                        stoneCubeObject.transform.position = new Vector3(x + OffsetX, Mathf.Round(curve.Evaluate(noiseMap[x, yStone]) * 5) - dirtStrate - i - 1, yStone);
                    }
                }

                var stoneCubeObjectLastStrate = Instantiate(stoneCube);
                blocs.Add(stoneCubeObjectLastStrate);
                stoneCubeObjectLastStrate.transform.position = new Vector3(x + OffsetX, Mathf.Round((noiseMap[x, yStone]) * -8) - dirtStrate - stoneStrate - 1, yStone);
            }



        }

        mapOrigin.x = (OffsetX + mapWidth) / (mapWidth *noiseScale);
        mapOrigin.y = (OffsetY + 0) / (mapHeight * noiseScale);
        OffsetX = OffsetX + mapWidth;
        OffsetY = OffsetY + 0;
    }

    public static float Perlin3DCavern(float x, float y, float z)
    {
        float AB = Mathf.PerlinNoise(x, y);
        float AC = Mathf.PerlinNoise(y, z);
        float CB = Mathf.PerlinNoise(x, z);
        float BA = Mathf.PerlinNoise(y, x);
        float CA = Mathf.PerlinNoise(z, y);
        float BC = Mathf.PerlinNoise(z, x);

        float ABC = (AB + AC + CB + BA + CA + BC);
        return ABC / 6;
    }



}
