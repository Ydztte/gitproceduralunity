﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProcedureWorld : MonoBehaviour
{
    #region
    public int mapWidth;
    public int mapHeight;
    public float scale;
    #endregion

    public int mapWidth2;
    public int mapHeight2;
    public float scale2;

    public Vector2 mapOrigin;

    public float[,] noiseMap = new float[0, 0];
    public float[,] noiseMap2 = new float[0, 0];

    public Renderer textureRender;
    public Renderer textureRender2;

    [Range(0f, 1f)]
    public float contrastMin;
    [Range(0f, 1f)]
    public float contrastMax;

    public Color colorMin;
    public Color colorMax;

    public float xGrille;
    public float yGrille;



    [HideInInspector]
    private Color[] colorMap;
    [HideInInspector]
    private Color[] colorMap2;

    public GameObject cube;
    public GameObject tree;


    public void Generate()
    {
        GenerateNoiseMap();


        DrawNoiseMap();


    }

    public void Start()
    {
        Generate();

        //GenerateFloor();

        //GenerateTree();
    }

    public void GenerateNoiseMap()
    {
        noiseMap = new float[mapWidth, mapHeight];
        colorMap = new Color[mapWidth * mapHeight];
        
        noiseMap2 = new float[mapWidth, mapHeight];
        colorMap2 = new Color[mapWidth * mapHeight];

        for (int i = 0; i < mapHeight; i++)
        {
            for (int j = 0; j < mapWidth; j++)
            {

                //ceration dune grille de float
                float xCoord = mapOrigin.x + i / (mapWidth * scale);
                float yCoord = mapOrigin.y + j / (mapHeight * scale);
                float sample = Mathf.PerlinNoise(xCoord, yCoord);

                //sample *= (contrastMax - contrastMin); cest la luminosité

                /*
                sample = Mathf.Clamp(sample, contrastMin, contrastMax);
                sample = Mathf.InverseLerp(contrastMin, contrastMax, sample);
                */


                noiseMap[i, j] = sample;

                
                Debug.Log("x =" + i);
                Debug.Log("y =" + j);
                


                //creation de la grille de couleur
                colorMap[j * mapWidth + i] = Color.Lerp(colorMin, colorMax, sample);

                /*
                //creation des arbres 
                if (Mathf.Floor(noiseMap[i, j] * 5) > 2) {
                    var newTree = Instantiate(tree);
                    newTree.transform.position =    new Vector3(i,
                                                                Mathf.Floor(noiseMap[i, j] * 5),
                                                                j );
                }*/


                //noiseMap[(int)j * tex.width + (int)i] = new Color(sample, sample, sample);

                

                for (int x = 0; x < mapHeight; x++)
                {
                    for (int y = 0; y < mapWidth; y++)
                    {

                        //ceration dune grille de float
                        float xCoord2 = mapOrigin.x + x / (mapWidth * scale);
                        float yCoord2 = mapOrigin.y + y / (mapHeight * scale);
                        float sample2 = Mathf.PerlinNoise(xCoord2, yCoord2);

                        //sample *= (contrastMax - contrastMin); cest la luminosité

                        /*
                        sample = Mathf.Clamp(sample, contrastMin, contrastMax);
                        sample = Mathf.InverseLerp(contrastMin, contrastMax, sample);
                        */

                        Debug.Log("x2 = " + x);
                        Debug.Log("y2 = " + y);

                        noiseMap2[x, y] = sample2;


                        



                        //creation de la grille de couleur
                        colorMap2[y * mapWidth + x] = Color.Lerp(colorMin, colorMax, sample2);


                        //creation des arbres 
                        if (Mathf.Floor(noiseMap[i, j]) > 2)
                        {
                            var newTree = Instantiate(tree);
                            newTree.transform.position = new Vector3(i,
                                                                        Mathf.Floor(noiseMap2[x, y] * 5),
                                                                        j);
                        }


                        //noiseMap[(int)j * tex.width + (int)i] = new Color(sample, sample, sample);
                    }
                }

            }
        }

        

    }

    public void DrawNoiseMap()
    {
        //Color[] colorMap = new Color[mapWidth * mapHeight];


        //creation dune nouvelle texture a partir de la grille de coueur
        Texture2D texture = new Texture2D(mapWidth, mapHeight);

        texture.SetPixels(colorMap);
        texture.Apply();

        //application sur le materiau d'un objet
        textureRender.sharedMaterial.mainTexture = texture;
        textureRender.transform.localScale = new Vector3(mapWidth, 1, mapHeight);

    }

    public void GenerateFloor()
    {
        for (int i = 0; i < mapWidth; i++)
        {
            for (int j = 0; j < mapHeight; j++)
            {
                var newCube = Instantiate(cube);
                newCube.transform.position = new Vector3(i, Mathf.Floor(noiseMap[i, j] * 5), j);
            }
        }
    }

}
