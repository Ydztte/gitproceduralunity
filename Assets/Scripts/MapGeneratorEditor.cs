﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ProcedureWorld))]
public class MapGeneratorEditor : Editor
{

    public override void OnInspectorGUI()
    {
        if (DrawDefaultInspector())
        {
            ProcedureWorld mapGen = (ProcedureWorld)target;
            mapGen.Generate();
        }



        
    }

}
