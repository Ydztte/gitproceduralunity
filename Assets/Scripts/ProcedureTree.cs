﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProcedureWorld01 : MonoBehaviour
{
    #region
    public int mapWidth;
    public int mapHeight;
    public float scale;
    #endregion

    public Vector2 mapOrigin;

    public float[,] noiseMap = new float[0, 0];

    public Renderer textureRender;

    [Range(0f,1f)]
    public float contrastMin;
    [Range(0f, 1f)]
    public float contrastMax;

    public Color colorMin;
    public Color colorMax;



    [HideInInspector]
    private Color[] colorMap;

    public GameObject tree;


    public void Generate()
    {
        GenerateNoiseMap();


        DrawNoiseMap();


    }

    public void Start()
    {
        Generate();
        GenerateTree();
    }

    public void GenerateNoiseMap()
    {
        noiseMap = new float[mapWidth, mapHeight];
        colorMap = new Color[mapWidth * mapHeight];

        for (int i = 0; i < mapHeight; i++)
        {
            for (int j = 0; j < mapWidth; j++)
            {

                //ceration dune grille de float
                float xCoord = mapOrigin.x + i / (mapWidth * scale);
                float yCoord = mapOrigin.y + j / (mapHeight * scale);
                float sample = Mathf.PerlinNoise(xCoord, yCoord);

                //sample *= (contrastMax - contrastMin); cest la luminosité

                sample = Mathf.Clamp(sample, contrastMin, contrastMax);
                sample = Mathf.InverseLerp(contrastMin, contrastMax, sample);



                noiseMap[i, j] = sample;


                Debug.Log("x" + i);
                Debug.Log("y" + j);



                //creation de la grille de couleur
                colorMap[j * mapWidth + i] = Color.Lerp(colorMin, colorMax, sample);


                //noiseMap[(int)j * tex.width + (int)i] = new Color(sample, sample, sample);
            }
        }
    }

    public void DrawNoiseMap()
    {
        //Color[] colorMap = new Color[mapWidth * mapHeight];


        //creation dune nouvelle texture a partir de la grille de coueur
        Texture2D texture = new Texture2D(mapWidth, mapHeight);

        texture.SetPixels(colorMap);
        texture.Apply();

        //application sur le materiau d'un objet
        textureRender.sharedMaterial.mainTexture = texture;
        textureRender.transform.localScale = new Vector3(mapWidth, 1, mapHeight);
    }


    public void GenerateTree()
    {
        for (int i = 0; i < mapWidth; i++)
        {
            for (int j = 0; j < mapHeight + 3; j++)
            {
                var newTree = Instantiate(tree);
                newTree.transform.position = new Vector3(i, Mathf.Floor(noiseMap[i, j] * 5) +2 , j);
            }
        }
    }
}
